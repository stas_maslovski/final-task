from pages.main_page import MainPage
from pages.login_page import LoginPage
from pages.cart_page import CartPage


def test_for_main_page(browser):
    main_page = MainPage(browser, MainPage.main_page_url)
    main_page.go_to_site()
    main_page.should_be_main_url()
    main_page.check_title()
    main_page.should_be_login()
    main_page.should_be_cart()
    main_page.should_be_search_form()
    main_page.should_be_best_sellers_block()
    main_page.should_be_categories()


def test_from_main_page_to_login(browser):
    main_page = MainPage(browser, MainPage.main_page_url)
    main_page.go_to_site()
    main_page.open_login_page()


def test_from_main_to_cart_page(browser):
    main_page = MainPage(browser, MainPage.main_page_url)
    main_page.go_to_site()
    main_page.open_cart_page()


def test_for_login_page(browser):
    login_page = LoginPage(browser, LoginPage.login_page_url)
    login_page.go_to_site()
    login_page.should_be_login_page_url()
    login_page.should_be_sign_in()
    login_page.should_be_sign_in_email()
    login_page.should_be_sign_in_passwd()
    login_page.should_be_create_account()
    login_page.complete_form_with_invalid_e_mail()
    login_page.complete_form_with_valid_e_mail()


def test_for_cart_page(browser):
    cart_page = CartPage(browser, CartPage.cart_page_url)
    cart_page.go_to_site()
    cart_page.should_be_cart_page_url()
    cart_page.message_cart()
    cart_page.should_be_return_to_home()


def test_add_item_to_cart(browser):
    main_page = MainPage(browser, MainPage.main_page_url)
    main_page.go_to_site()
    main_page.open_women_category()
    main_page.add_item_to_cart()
    main_page.should_be_icon_ok()
    main_page.proceed_to_checkout()
    main_page.check_cart_is_not_empty()


def test_add_to_wishlist(browser):
    login_page = LoginPage(browser, LoginPage.login_page_url)
    login_page.go_to_site()
    login_page.complete_form_with_valid_e_mail()
    main_page = MainPage(browser, MainPage.main_page_url)
    main_page.go_to_site()
    main_page.add_item_to_whishlist()
