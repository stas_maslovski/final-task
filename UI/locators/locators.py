from selenium.webdriver.common.by import By


class Locators:
    # main
    LOGIN = (By.CLASS_NAME, 'login')
    CART = (By.XPATH, '//*[@title="View my shopping cart"]')
    SEARCH_FORM = (By.ID, 'search_query_top')
    CATEGORIES = (By.CLASS_NAME, 'cat-title')
    BEST_SELLERS = (By.CLASS_NAME, 'blockbestsellers')
    ADD_TO_CART = (By.XPATH, '//*[@title="Add to cart", "]')
    DRESSES = (By.CSS_SELECTOR, '# block_top_menu > ul > li:nth-child(2) > a')
    CATEGORY_WOMEN = (By.XPATH, '//*[@title="Women"]')
    BUTTON_ADD_ITEM = (By.CSS_SELECTOR, '[data-id-product="1"]')
    MORE_BUTTON = (By.CSS_SELECTOR, '*.button lnk_view btn btn-default')
    MORE = (By.XPATH, '//*[@id="homefeatured"]/li[1]/div/div[2]/div[2]/a[2]')
    WISH_LIST_BUTTON = (By.ID, 'wishlist_button')
    MESSAGE_YOU_MAST_BE_LOGGED = (By.CLASS_NAME, 'fancybox-error')
    ADD_TO_WISH_LIST = (By.CLASS_NAME, 'addToWishlist wishlistProd_1')
    QUICK_VIEW = (By.CLASS_NAME, 'replace-2x img-responsive')

    # login

    SIGN_IN = (By.ID, 'SubmitLogin')
    SIGN_IN_E_MAIL = (By.ID, 'email')
    SIGN_IN_PASSWD = (By.ID, 'passwd')
    CREATE_ACCOUNT_FORM = (By.ID, 'email_create')
    CREATE_BUTTON = (By.ID, 'SubmitCreate')
    CREATE_ACCOUNT_ERROR = (By.CSS_SELECTOR,
                            '#create_account_error > ol > li')

    # cart
    MESSAGE_EMPTY_CART = (By.CSS_SELECTOR, '#center_column > p')
    RETURN_TO_HOME = (By.CLASS_NAME, 'home')
    ICON_OK = (By.CLASS_NAME, 'icon-ok')
    ITEM = (By.CLASS_NAME, 'alt="Faded Short Sleeve T-shirts"')
    PROCEED_TO_CHECKOUT = \
        (By.XPATH, "//*[@class='btn btn-default button button-medium']")
    SHOPPING_CART_MESSAGE = (By.ID, 'summary_products_quantity')
    ORDER = (By.XPATH,
             "//*[@class='order hide-left-column hide-right-column lang_en']")



