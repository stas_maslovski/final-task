from UI.locators.locators import Locators
from UI.pages.base_page import BasePage
from UI.data import data_for_tests


class LoginPage(BasePage):
    login_page_url = 'http://automationpractice.com/' \
                     'index.php?controller=authentication&back=my-account'
    account_creation_url = "http://automationpractice.com/" \
                           "index.php?controller=authentication&back" \
                           "=my-account"

    def should_be_login_page_url(self):
        assert self.driver.current_url == self.login_page_url

    def should_be_sign_in(self):
        assert self.exist_check(Locators.SIGN_IN)

    def should_be_sign_in_email(self):
        assert self.exist_check(Locators.SIGN_IN_E_MAIL)

    def should_be_sign_in_passwd(self):
        assert self.exist_check(Locators.SIGN_IN_PASSWD)

    def should_be_create_account(self):
        assert self.exist_check(Locators.CREATE_ACCOUNT_FORM)

    def complete_form_with_valid_e_mail(self):
        form = self.find_elem(Locators.CREATE_ACCOUNT_FORM)
        form.send_keys(data_for_tests.valid_e_mail)
        form.submit()
        assert self.driver.current_url == self.account_creation_url

    def complete_form_with_invalid_e_mail(self):
        form = self.find_elem(Locators.CREATE_ACCOUNT_FORM)
        form.send_keys(data_for_tests.invalid_e_mail)
        form.submit()
        message = self.find_elem(Locators.CREATE_ACCOUNT_ERROR).text
        assert message == data_for_tests.error_message
