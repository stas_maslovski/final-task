from UI.data import data_for_tests
from UI.locators.locators import Locators
from UI.pages.base_page import BasePage
from UI.pages.main_page import MainPage


class CartPage(BasePage):
    cart_page_url = 'http://automationpractice.com/' \
                    'index.php?controller=order'

    def should_be_cart_page_url(self):
        assert self.driver.current_url == self.cart_page_url

    def message_cart(self):
        msg = self.find_elem(Locators.MESSAGE_EMPTY_CART).text
        assert msg == data_for_tests.message_empty_cart

    def should_be_return_to_home(self):
        self.find_elem(Locators.RETURN_TO_HOME).click()
        assert self.driver.current_url == MainPage.main_page_url




