import time

from UI.data import data_for_tests
from UI.locators.locators import Locators
from UI.pages.base_page import BasePage


class MainPage(BasePage):
    main_page_url = 'http://automationpractice.com/index.php'
    women_category_url = 'http://automationpractice.com/index.php' \
                         '?id_category=3&controller=category'

    def open_login_page(self):
        button_login = self.find_elem(Locators.LOGIN)
        button_login.click()
        assert self.driver.current_url == \
               'http://automationpractice.com/' \
               'index.php?controller=authentication&back=my-account'

    def open_cart_page(self):
        button_login = self.find_elem(Locators.CART)
        button_login.click()
        assert self.driver.current_url == \
               'http://automationpractice.com/' \
               'index.php?controller=order'

    def check_title(self):
        assert data_for_tests.title in self.driver.title

    def should_be_main_url(self):
        assert self.driver.current_url == self.url

    def should_be_login(self):
        assert self.exist_check(Locators.LOGIN)

    def should_be_cart(self):
        assert self.exist_check(Locators.CART)

    def should_be_search_form(self):
        assert self.exist_check(Locators.CART)

    def should_be_best_sellers_block(self):
        assert self.exist_check(Locators.BEST_SELLERS)

    def should_be_categories(self):
        assert self.exist_check(Locators.CATEGORIES)

    def open_women_category(self):
        self.find_elem(Locators.CATEGORY_WOMEN).click()
        assert self.driver.current_url == self.women_category_url

    def add_item_to_cart(self):
        self.find_elem(Locators.BUTTON_ADD_ITEM).click()

    def should_be_icon_ok(self):
        assert self.exist_check(Locators.ICON_OK)

    def proceed_to_checkout(self):
        self.find_elem(Locators.PROCEED_TO_CHECKOUT).click()

    def check_cart_is_not_empty(self):
        text1 = self.find_elem(Locators.SHOPPING_CART_MESSAGE).text
        assert text1 == '1 Product'
        assert self.exist_check(Locators.ORDER)

    def add_item_to_whishlist(self):
        self.find_elem(Locators.CATEGORY_WOMEN).click()
        self.find_elem(Locators.MORE).click()
        self.find_elem(Locators.ADD_TO_WISH_LIST).click()
        assert self.exist_check(Locators.MESSAGE_YOU_MAST_BE_LOGGED)
