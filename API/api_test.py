import pytest
import requests
import json

with open('data.json', 'r') as file:
    data = json.load(file)


@pytest.mark.positive
def test_checking_that_endpoint_is_reachable(url=data["URL"]):
    response = requests.get(url).json()
    assert response['code'] == 200, \
        f"Failed to request. Response: {response}"


@pytest.mark.positive
def test_creating_user(auth_token=data['auth_header'], url=data["URL"]):
    response = requests.post(url, headers=auth_token, data=data['payload'])
    response_json = response.json()
    response_id = response_json['data']['id']
    print(f'The userID generated is: {response_id}')
    assert response_json['code'] == 201, \
        f"Failed to request. Response: {response}"
    assert response_json['data']['name'] == data['name']
    assert response_json['data']['email'] == data['email']
    assert response_json['data']['gender'] == "Male"
    assert response_json['data']['status'] == "Active"


@pytest.mark.positive
def test_get_information_by_id(url=data['URL'], id=100):
    response = requests.get(f'{url}/{id}')
    response_json = response.json()
    assert response_json['code'] == 200, \
        f"Failed to request. Response: {response}"
    print(f'''
Name: {response_json['data']['name']}
e-mail: {response_json['data']['email']}
gender: {response_json['data']['gender']}
status: {response_json['data']['status']}'''
          )


@pytest.mark.negative
def test_faulty_payload(auth_token=data['auth_header'],
                        url=data['URL'], inv_data=data['invalid_payload']):
    response = requests.post(url, headers=auth_token, data=inv_data).json()
    assert response['code'] == 422


@pytest.mark.negative
def test_deleting_nonexistent_user\
                (auth_token=data['auth_header'], url=data['URL']):
    response = requests.delete(f'{url}/1235484', headers=auth_token).json()
    assert response['code'] == 404


@pytest.mark.negative
def test_faulty_auth_key(faulty_auth_token=data['auth_header_inv'],
                         url=data['URL']):
    response = requests.post(url, headers=faulty_auth_token,
                             data=data['payload'])
    response_json = response.json()
    assert response_json['code'] == 401 and \
           response_json['data']['message'] == "Authentication failed"
