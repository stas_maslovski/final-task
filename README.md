### UI test

To run the tests you have to download this repository and install:
 
- Python 3.6+
- Chrome Browser
- Chrome driver

python3 - pip https://www.python.org/downloads/ 
Chrome Browser https://www.google.com/chrome
Chrome driver https://chromedriver.chromium.org/downloads

Requirements:
pytest
selenium

You can run the tests from command line interface:
pytest -v  path to file/tests.py


## REST API testing 

To run the tests you have to download this repository and install Python 3.6+
python3 - pip https://www.python.org/downloads/

Requirements: 
python3
pytest
requests

To run the tests, you should do the following commands in the command line:


- To run the positive scenarios, please run: `pytest -m positive -v 
- To run the negative scenarios, please run: `pytest -m negative -v 
